/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import Navigation from './src/navigation/navigation'; 
import FlashMessage from "react-native-flash-message";

import AuthContextProvider from './src/context//auth-context'

const App = () => {
    return (
        <AuthContextProvider>
            <Navigation />
            <FlashMessage position="top" />
        </AuthContextProvider>
    );
};

export default App;