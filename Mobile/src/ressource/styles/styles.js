import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
const {width: WIDTH} = Dimensions.get('window')
const {height: HEIGHT} = Dimensions.get('window')

const percentWidth = (num) => {
    let percent = (WIDTH * num) / 100;
    return percent
}

const percentHeight = (num) => {
    let percent = (HEIGHT * num) / 100;
    return percent
}

export default Styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        paddingLeft: percentWidth(5),
        paddingRight: percentWidth(5),
        backgroundColor: "#ffffff"
    },
    infoContainer: {
        flex: 1,
        paddingLeft: percentWidth(6),
        paddingRight: percentWidth(6),
        backgroundColor: "#ffffff"
    },
    loginContainer: {
        flex: 1,
    },
    headerLogin: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#066d68',
        borderBottomWidth: 5,
        borderBottomColor: "#619e27" ,
    },
    containerLogin: {
        flex: 2,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: percentWidth(10),
        paddingRight: percentWidth(10),
    },
    loginInputContainer: {
        // marginTop: 10,
        marginBottom: 10,
        // backgroundColor: '#bbbaba67',
    },
    loginInput: {
        width: percentWidth(80),
		height: 50,
		fontSize: 18,
		paddingLeft: 50,
		color: '#000',
		borderBottomColor: '#000',
		borderBottomWidth: 0.19,
		marginBottom: 5,
		marginTop: 5,
		marginHorizontal: 10,
    },
    inputIcon:{
		position: 'absolute',
		top: 15,
		left: 20
    },
    registerInput: {
        width: percentWidth(80),
		height: 50,
		fontSize: 15,
		paddingLeft: 20,
		color: '#000',
		borderBottomColor: '#000',
		borderBottomWidth: 0.19,
		marginBottom: 5,
		marginTop: 5,
		marginHorizontal: 10,
    },
    btnLogin: {
		width: percentWidth(80),
		height: 35,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 10,
        backgroundColor: '#619e27',
        borderRadius: 20
    },
    otherLogin:{
        width: percentWidth(80),
        paddingLeft: 10,
        paddingRight: 10,
        flexDirection: 'row',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        fontSize: 15
    },
    loading_container: {
		flex: 2,
		alignItems: 'center',
		justifyContent: 'center'
	},
    head: {
        height: 35,
        borderBottomColor: "#9999998f",
        borderBottomWidth: 1,
        paddingTop: 10,
        flexDirection: "row"
    },
    footer: {
        height: 70,
        borderTopWidth: 1,
        borderTopColor: '#9999998f',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    headText: {
        color: "#1ca7a0",
        fontWeight: "bold",
        flex: 2
    },
    refresh: {
        flex: 0.3,
        marginRight: 5
    },
    panelDisplay: {
        flex: 4,
        paddingTop: 20,
        marginBottom: 15
    },
    list: {
        flex: 1,
    },
    listItem: {
        flex: 1,
        flexDirection: "row",
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 50,
        paddingRight: 50,
        height: 30,
    },
    listItemBasket: {
        flex: 1,
        flexDirection: "row",
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 30,
        paddingRight: 30,
        borderBottomColor: "#9999998f",
        borderBottomWidth: 1
    },
    listItemShop: {
        flex: 1,
        paddingBottom: 5,
    },
    listItemDate: {
        color: "#1ca7a0",
        flex: 2,
    },
    listItemHeure: {
        color: "#1ca7a0",
        flex: 1,
    },
    listCheckDetail: {
        flex: 1,
    },
    textInput:{
		height: 40,
		fontSize: 15,
		color: '#000',
		borderBottomColor: '#535353',
		borderBottomWidth: 0.19,
        marginBottom: 10,
        marginTop: 5,
        backgroundColor: '#e0e0e03d'
    },
    btnEye:{
		position: 'absolute',
		top: 17,
		right: 25
    },
    btnUpdate: {
		height: 30,
		justifyContent: 'center',
		alignItems: 'center',
        marginTop: 20,
        marginLeft: percentWidth(30),
        marginRight: percentWidth(30),
        backgroundColor: '#619e27',
        borderRadius: 15
    },
    btnUpdateOrange: {
		height: 30,
		justifyContent: 'center',
		alignItems: 'center',
        marginTop: 20,
        marginLeft: percentWidth(30),
        marginRight: percentWidth(30),
        backgroundColor: '#ec7e49',
        borderRadius: 15
    },
    btnUpdateVert: {
		height: 30,
		justifyContent: 'center',
		alignItems: 'center',
        marginTop: 20,
        width: percentWidth(30),
        // marginRight: percentWidth(30),
        backgroundColor: '#619e27',
        borderRadius: 15
    },
    btnUpdateRed: {
		height: 30,
		justifyContent: 'center',
		alignItems: 'center',
        marginTop: 20,
        width: percentWidth(30),
        // marginRight: percentWidth(30),
        backgroundColor: 'red',
        borderRadius: 15
    },
    avatar: {
        flex: 1,
        borderRadius: 100,
		height: 80,
        width: 80,
        marginLeft: percentWidth(33),
        marginRight: percentWidth(33),
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#535353',
		marginBottom: 30
    },
    imageDetail: {
        flex: 1,
        height: 400
    },
    
    scrollview_container: {
        flex: 1,
        marginTop: 10,
        paddingLeft: 20,
        paddingRight: 20
    },
    title_container:{
		// flex: 0.2,
        marginTop: 10,
        flexDirection: 'row',
        height: 30,
	},
	title: {
        flex: 1,
        fontSize: 15,
        color: "#1ca7a0",
        
    },
    prix: {
        flex: 1,
        textAlign: 'right',
        fontSize: 15,
        color: "#ec7e49"
    },
    description: {
		textAlign: 'justify',
    },
    actionSell: {
        height: percentHeight(7),
        backgroundColor: '#1ca7a0',
    }, 
    actionSection: {
        flex: 1, 
        flexDirection: 'row', 
        alignItems: "center", 
        paddingTop: 15, 
        paddingBottom: 15,
        paddingLeft: 20,
        paddingRight: 20
    },
    btnAdd: {
        backgroundColor: '#066d68',
        borderRadius: 15,
        width: 20,
        height: 20, 
    },
    btnBuy: {
        backgroundColor: '#066d68',
        borderRadius: 15,
        width: 140,
        height: 30, 
        justifyContent: 'center'
    },
    imageBracketList: {
        flex: 1,
        width: 80,
        height: 80
    },
    productInfo: {
        flex: 1,
        marginLeft: 5
    },
    imageContainer: {
        paddingBottom: 5, 
        paddingTop: 5, 
        paddingRight: 5, 
        paddingLeft: 5,
    },
    totalPrice: {
        backgroundColor: '#bbbaba67',
        height: percentHeight(7),
    },
    btnCommandSection: {
        alignItems: 'center',
        justifyContent: 'center',
        height: percentHeight(10),
    },
    btnCommand: {
        width: percentWidth(25),
        height: 30, 
        backgroundColor: "#ec7e49",
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
        marginRight: 10
    },
    btnUpdateCommand: {
        width: percentWidth(25),
        height: 30, 
        backgroundColor: "#12837d",
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
    }
})