import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';

import { Notification } from '../components/layout/notification';
import { urlServer } from '../ressource/api/Url';

export const AuthContext = React.createContext()

class AuthContextProvider extends React.Component{

	state = {
		idUser: null,
		token: null,
	} 
	
	login = async (idUser, token) => {
		this.setState({
			token: token,
			idUser: idUser,
        })
        this.storeToken()
	}

	logout = async () => {
        try{
            await AsyncStorage.removeItem('user-auth')
        }catch(e){
            console.log(e)
        }
		this.setState({
			token: null, 
			idUser: null
		})
    }
    
    storeToken = async () => {
        try {
            await AsyncStorage.setItem('user-auth', JSON.stringify(this.state))
        } catch (e) {
            console.log(e)
        }
    }

    setToken = async (token) => {
        await this.setState({
            token: token.token,
            idUser: token.idUser,
        })
    }

    getToken = async () => {
        try{
            const userAuth = await AsyncStorage.getItem('user-auth')
            if(userAuth){
                const jsonvalue = JSON.parse(userAuth)
                return jsonvalue
            }
        }catch(e){
            console.log(e)
        }
    }

	render(){ 
		return(
            <AuthContext.Provider 
                value={{ 
                    ...this.state, 
                    login: this.login, 
                    logout: this.logout, 
                    setToken: this.setToken ,
                    getToken: this.getToken
                }}
            >
				{ this.props.children }
			</AuthContext.Provider>
		)
	}
}

export default AuthContextProvider