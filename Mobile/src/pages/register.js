import React from 'react';
import { 
    View, 
    TextInput, 
    TouchableOpacity, 
    Image, 
    StatusBar, 
    Text, 
    KeyboardAvoidingView, 
    ScrollView,
    SafeAreaView,
    Picker,
    ActivityIndicator,

} from 'react-native';
// import { Picker } from '@react-native-community/picker';
import countryList from 'react-select-country-list';
import Ionicon from 'react-native-vector-icons/Ionicons';

import Styles from '../ressource/styles/styles';
import { registerformvalidation } from '../components/layout/form-validation'
import { urlServer } from '../ressource/api/Url';
import { Notification } from '../components/layout/notification';

class Register extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            keyHide: true,
            press: false,
            country: countryList().getData(),
            countryOptions: null,
            isLoading: false,
        }
        this.name = null 
        this.lastname = null 
        this.email = null 
        this.phone = null 
        this.password = null 
        this.c_password = null 
        this.adress = null
    }

    _showPassword = () => {
		if(this.state.press == false){
			this.setState({
				keyHide: false,
				press: true,
			});
		}else{
			this.setState({
				keyHide: true,
				press: false,
			});
		}
    };

    _register = async () => {
        this.setState({
            isLoading: true
        })

        const bodyRequest = {
            name: this.name,
            lastname: this.lastname,
            email: this.email,
            phone: this.phone,
            country: this.state.countryOptions,
            password: this.password,
            c_password: this.c_password,
            address: this.adress,
            type: 1
        }

        let validation = await registerformvalidation(bodyRequest)

        if(validation){
            let register = await fetch(urlServer+'register', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(bodyRequest)
            }).then(res => {
                return res.json()
            }).catch(err => {
                Notification({
                    type: 'danger', 
                    message: 'Une erreur s\'est produite, veuillez vérifier votre connexion',
                    duration: 5000
                })
            })
            if(register && register.success){
                const notif = {
                    type: 'success',
                    message: 'Création de compte réussi',
                    duration: 5000
                }
                Notification(notif)
                setTimeout(() => {
                    this.props.navigation.reset({
                        index: 0,
                        routes: [{name: 'Login'}]
                    })
                },1000)
            }else{
                Notification({
                    type: 'danger', 
                    message: 'L\'addresse email éxiste déjà',
                    duration: 5000
                })
            }
            this.setState({
                isLoading: false
            })
        }
        this.setState({
            isLoading: false
        })
    }

    _displayLoading = () => {
		if(this.state.isLoading){
			return (
				<View style={styles.btnLogin}>
                    <ActivityIndicator size="large" color="#fff" />
                </View>
			);
		}else{
			return (
				<TouchableOpacity style={styles.btnLogin} onPress={() => this._register()}>
                    <Text style={{ color: '#fff', fontSize: 18 }}>S'inscrire</Text>
                </TouchableOpacity>
			);
		}
	};
    
    render(){
        return(
            <KeyboardAvoidingView 
                behavior={'height'}
                keyboardVerticalOffset={-150}
                style={[styles.loginContainer]}
            >
                <StatusBar backgroundColor="#066d68"/>
                <View style={[styles.headerLogin, { flex: 0.6 }]}>
                    <Image 
                        style={{width: 160, height: 100,}}
                        source={require('../ressource/images/log.png')}
                    />
                </View>
                <SafeAreaView style={[styles.containerLogin, {paddingTop: 10, paddingBottom: 10}]}>
                    <ScrollView>
                        <View style={styles.loginInputContainer}>
                            <Text style={{color: "#535353", fontSize: 15, paddingLeft: 10}}>Nom</Text>
                            <TextInput
                                placeholder="Nom"
                                placeholderTextColor={'#bbbaba67'}
                                style={styles.registerInput}
                                onChangeText={value => (this.name = value)}
                            />
                        </View>
                        <View style={styles.loginInputContainer}>
                            <Text style={{color: "#535353", fontSize: 15, paddingLeft: 10}}>Prénom</Text>
                            <TextInput
                                placeholder="Prénom"
                                placeholderTextColor={'#bbbaba67'}
                                style={styles.registerInput}
                                onChangeText={value => (this.lastname = value)}
                            />
                        </View>
                        <View style={styles.loginInputContainer}>
                            <Text style={{color: "#535353", fontSize: 15, paddingLeft: 10}}>E-mail</Text>
                            <TextInput
                                placeholder="E-mail"
                                placeholderTextColor={'#bbbaba67'}
                                style={styles.registerInput}
                                onChangeText={value => (this.email = value)}
                            />
                        </View>
                        <View style={styles.loginInputContainer}>
                            <Text style={{color: "#535353", fontSize: 15, paddingLeft: 10}}>Téléphone</Text>
                            <TextInput
                                placeholder="Téléphone"
                                placeholderTextColor={'#bbbaba67'}
                                style={styles.registerInput}
                                onChangeText={value => (this.phone = value)}
                            />
                        </View>
                        <View style={styles.loginInputContainer}>
                            <Text style={{color: "#535353", fontSize: 15, paddingLeft: 10}}>Pays</Text>
                            <View style={styles.registerInput}>
                                <Picker
                                    selectedValue={this.state.countryOptions}
                                    onValueChange={(itemValue, itemIndex) => this.setState({countryOptions: itemValue})}
                                >
                                    <Picker.Item 
                                        label="" 
                                        value="" 
                                        key={0}
                                    />
                                    { this.state.country.map((item, key = 0) => {
                                        key += 1
                                        return(
                                            <Picker.Item 
                                                label={item.label} 
                                                value={item.label} 
                                                key={key}
                                            />
                                        )
                                    }) }
                                    <Picker.Item label="Java" value="java" />
                                    <Picker.Item label="JavaScript" value="js" />
                                </Picker>
                            </View>
                        </View>
                        <View style={styles.loginInputContainer}>
                            <Text style={{color: "#535353", fontSize: 15, paddingLeft: 10}}>Addresse</Text>
                            <TextInput
                                placeholder="Addresse"
                                placeholderTextColor={'#bbbaba67'}
                                style={styles.registerInput}
                                onChangeText={value => (this.adress = value)}
                            />
                        </View>
                        <View style={styles.loginInputContainer}>
                            <Text style={{color: "#535353", fontSize: 15, paddingLeft: 10}}>Mot de passe</Text>
                            <TextInput
                                placeholder="(8 charactère minimum)"
                                placeholderTextColor={'#bbbaba67'}
                                secureTextEntry={this.state.keyHide}
                                style={styles.registerInput}
                                onChangeText={value => (this.password = value)}
                            />
                            <TouchableOpacity
                                style={styles.btnEye}
                                onPress={this._showPassword.bind(this)}>
                                <Ionicon
                                    name={
                                        this.state.press == false ? 'md-eye' : 'md-eye-off'
                                    }
                                    size={20}
                                    color="#066d68"
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.loginInputContainer}>
                            <Text style={{color: "#535353", fontSize: 15, paddingLeft: 10}}>Confirmation mot de passe</Text>
                            <TextInput
                                placeholder="••••••••"
                                placeholderTextColor={'#bbbaba67'}
                                secureTextEntry={this.state.keyHide}
                                style={styles.registerInput}
                                onChangeText={value => (this.c_password = value)}
                            />
                            <TouchableOpacity
                                style={styles.btnEye}
                                onPress={this._showPassword.bind(this)}>
                                <Ionicon
                                    name={
                                        this.state.press == false ? 'md-eye' : 'md-eye-off'
                                    }
                                    size={20}
                                    color="#066d68"
                                />
                            </TouchableOpacity>
                        </View>
                        { this._displayLoading() }
                    </ScrollView>
                </SafeAreaView>
            </KeyboardAvoidingView>
        )
    }
}

const styles = Styles
export default Register