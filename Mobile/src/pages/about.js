import React from 'react';
import { View, Text, Linking, Image } from 'react-native';
import Fonticon from 'react-native-vector-icons/FontAwesome';

import Styles from '../ressource/styles/styles';

class About extends React.Component{

    constructor(props) {
	    super(props)
    }
    render(){
        return(
            <View style={styles.infoContainer}>
                <View style={styles.head}>
                    <Text style={styles.headText}>A propos d'Alt. Soap</Text>
                </View>
                <View style={{flex: 2, marginTop: 20}}>
                    <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold' }}>
                        Qui somme nous
                    </Text>
                    <Text style={{ textAlign: 'center', marginTop: 15, color: "#535353" }}>
                        Association et entreprise social lancée en 2016
                    </Text>
                    <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>
                        Green N Kool
                    </Text>
                    <Text style={{ textAlign: 'center', color: "#535353" }}>
                        Best social startup 2019  à la southrn  African Youth Awards
                    </Text>
                    <Text style={{ textAlign: 'center', marginTop: 15, color: "#535353" }}>
                        1 er prix du Hackaton National "Hack the crisis, Hackoragnavirus" avec le projet Alt. Soap
                    </Text>
                    <Text style={{ textAlign: 'center', marginTop: 15, color: "#535353" }}>
                        5ème dans la catégorie d'environnement au Global Hack 
                        plus grand Hackaton mondial (800 projets de plus de  60 pays)
                        avec le projet Alt. Soap
                    </Text>
                    <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold', marginTop: 30 }}>
                        Pour plus d'information, nous vous invitons de visiter le site
                    </Text>
                    <Text 
                        onPress={() => Linking.openURL('https://green-n-kool.jimdofree.com/')}
                        style={{ textAlign: 'center', fontSize: 16, color: "#1ca7a0", textDecorationLine: 'underline', }}
                    >
                        green-n-kool.jimdo.com
                    </Text>
                    <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold', marginTop: 20 }}>
                        ou appeler le numero
                    </Text>
                    <Text 
                        onPress={() => Linking.openURL('tel: +261 34 55 00 214')}
                        style={{ textAlign: 'center', fontSize: 16, color: "#1ca7a0", textDecorationLine: 'underline', }}
                    >
                        +261 34 55 00 214
                    </Text>

                    <Text style={{ textAlign: 'center', fontSize: 16, color: "#1ca7a0", marginTop: 20 }}>
                        Think Green Madagascar
                    </Text>

                </View>
                <View style={styles.footer} >
                    <Image 
                        style={{width: 80, height: 45}}
                        source={require('../ressource/images/logo-altsoap.png')}
                    />
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
                        <Fonticon 
                            size={30}
                            color="#12837d"
                            name="facebook-official"
                            style={{marginLeft: 10}}
                        />
                        <Fonticon 
                            size={30}
                            color="#12837d"
                            name="twitter"
                            style={{marginLeft: 10}}
                        />
                        <Fonticon 
                            size={30}
                            color="#12837d"
                            name="instagram"
                            style={{marginLeft: 10}}
                        />

                    </View>
                </View>
            </View>
        )
    }
}

const styles = Styles
export default About