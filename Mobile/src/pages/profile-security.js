import React from 'react';
import { View, Text, TextInput, KeyboardAvoidingView, TouchableOpacity, ActivityIndicator } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';


import { AuthContext } from '../context/auth-context';
import Styles from '../ressource/styles/styles';
import { urlServer } from '../ressource/api/Url';
import { Notification } from '../components/layout/notification';
import { passwordformvalidation } from '../components/layout/form-validation';

class ProfileSecurity extends React.Component{

    static contextType = AuthContext;

    constructor(props) {
        super(props)
        this.state = {
            isSecurityLoading: false,
            keyHide: true,
            press: false,
        }
        this.oldpass = null;
        this.newpass = null;
        this.passconfirm = null;
    }

    _showPassword = () => {
		if(this.state.press == false){
			this.setState({
                keyHide: false,
				press: true,
			});
		}else{
			this.setState({
				keyHide: true,
				press: false,
			});
		}
    };
    
    _updatePassword = async () => {
        this.setState({
            isSecurityLoading: true,
        })
        const bodyRequestSecurity = {
            oldpassword: this.oldpass,
            newpassword: this.newpass,
            passconfirm: this.passconfirm
        }
        let validation = await passwordformvalidation(bodyRequestSecurity)
        if(validation){
            let data = await fetch(urlServer+'user/'+this.context.idUser+'', {
                method: 'PUT',
                headers: {
                    Authorization: 'Bearer '+this.context.token,
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(bodyRequestSecurity)
            }).then(res => {
                return res.json()
            }).catch(err => {
                Notification({
                    type: 'danger', 
                    message: 'Une erreur s\'est produite, veuillez vérifier votre connexion et réessayer',
                    duration: 5000
                })
            })
            if(data.error){
                Notification({
                    type: 'danger', 
                    message: 'Mot de passe érroné',
                    duration: 5000
                })
                this.setState({
                    isSecurityLoading: false,
                })
            }else{
                this.setState({
                    isSecurityLoading: false,
                })
                Notification({
                    type: 'success', 
                    message: 'Modification mot de passe réussie',
                    duration: 5000
                })
            }
        }else{
            this.setState({
                isSecurityLoading: false,
            })
        }
    }
    
    _displayLoadingSecurity = () => {
        if(this.state.isSecurityLoading){
            return (
				<View style={styles.btnUpdate}>
                    <ActivityIndicator size="large" color="#fff" />
                </View>
			);
        }else{
            return(
                <TouchableOpacity 
                    style={styles.btnUpdate}
                    onPress={() => this._updatePassword()}
                >
                    <Text style={{color: "#ffffff"}}>Modifier</Text>
                </TouchableOpacity>
            )
        }
    }

    render(){
        return(
            <View style={styles.mainContainer}>
                <View style={styles.head}>
                    <Text style={styles.headText}>Sécurité</Text>
                </View>
                <KeyboardAvoidingView 
                    style={styles.panelDisplay}
                >
                    <Text style={{color: "#535353"}}>Ancien mot de passe</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            placeholder="••••"
                            placeholderTextColor={'#5353533f'}
                            underlineColorAndroid="transparent"
                            secureTextEntry={this.state.keyHide}
                            style={styles.textInput}
                            onChangeText={value => (this.oldpass = value)}
                        />
                        <TouchableOpacity
                            style={styles.btnEye}
                            onPress={this._showPassword.bind(this)}>
                            <Ionicon
                                name={
                                    this.state.press == false ? 'md-eye' : 'md-eye-off'
                                }
                                size={20}
                                color="#000"
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={{color: "#535353"}}>Nouveau mot de passe</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            placeholder="••••"
                            placeholderTextColor={'#5353533f'}
                            underlineColorAndroid="transparent"
                            secureTextEntry={this.state.keyHide}
                            style={styles.textInput}
                            onChangeText={value => (this.newpass = value)}
                        />
                        <TouchableOpacity
                            style={styles.btnEye}
                            onPress={this._showPassword.bind(this)}>
                            <Ionicon
                                name={
                                    this.state.press == false ? 'md-eye' : 'md-eye-off'
                                }
                                size={20}
                                color="#000"
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={{color: "#535353"}}>Confirmer mot de passe</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            placeholder="••••"
                            placeholderTextColor={'#5353533f'}
                            underlineColorAndroid="transparent"
                            secureTextEntry={this.state.keyHide}
                            style={styles.textInput}
                            onChangeText={value => (this.passconfirm = value)}
                        />
                        <TouchableOpacity
                            style={styles.btnEye}
                            onPress={this._showPassword.bind(this)}>
                            <Ionicon
                                name={
                                    this.state.press == false ? 'md-eye' : 'md-eye-off'
                                }
                                size={20}
                                color="#000"
                            />
                        </TouchableOpacity>
                    </View>
                    { this._displayLoadingSecurity() }
                </KeyboardAvoidingView>
            </View>
        )
    }
}

const styles = Styles
export default ProfileSecurity