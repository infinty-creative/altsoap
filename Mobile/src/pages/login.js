import React from 'react';
import { View, TextInput, TouchableOpacity, Image, StatusBar, Text, KeyboardAvoidingView, ActivityIndicator } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';

import { AuthContext } from '../context/auth-context';
import { Notification } from '../components/layout/notification';
import { loginformvalidation } from '../components/layout/form-validation'
import { urlServer } from '../ressource/api/Url';

import Styles from '../ressource/styles/styles';

class Login extends React.Component{

    static contextType = AuthContext;

    constructor(props) {
        super(props)
        this.state = {
            keyHide: true,
            press: false,
            isLoading: false,
            verify: false
        }
        this.email = null 
        this.password = null
    }

    async componentDidMount(){
        this.setState({
            verify: true
        })
        let token = await this.context.getToken()
        if(token){
            const validation = await this._verifyToken(token)
            if(validation){
                await this.context.setToken(token)
            }else{
                await this.context.logout()
            }
        }
        this.setState({
            verify: false
        })
    }

    _verifyToken = async (token) => {
        return await fetch(urlServer+'user/'+token.idUser, {
            method: 'GET',
            headers: {
                Authorization: 'Bearer '+token.token
            }
        }).then(res => {
            return res.json()
        }).catch(err => {
            Notification({
                type: 'danger', 
                message: 'Erreur login, veuillez reconnécter',
                duration: 5000
            })
        })
    }

    _displayLoadingVerification = () => {
        if(this.state.verify){
			return(
				<View style={[styles.loading_container]}>
					<ActivityIndicator size="large" color="#1ca7a0"/>
				</View>
			)
		}else{
            return(
                <View style={styles.containerLogin}>
                    <View style={styles.loginInputContainer}>
                        <Ionicon
                            name="md-person"
                            size={30}
                            color="#066d68"
                            style={styles.inputIcon}
                        />
                        <TextInput
                            placeholder={'E-mail'}
                            placeholderTextColor={'#bbbaba67'}
                            style={styles.loginInput}
                            onChangeText={value => (this.email = value)}
                        />
                    </View>
                    <View style={styles.loginInputContainer}>
                        <Ionicon
                            name="md-lock"
                            size={30}
                            color="#066d68"
                            style={styles.inputIcon}
                        />
                        <TextInput
                            placeholder="Mot de passe"
                            placeholderTextColor={'#bbbaba67'}
                            secureTextEntry={this.state.keyHide}
                            style={styles.loginInput}
                            onChangeText={value => (this.password = value)}
                        />
                        <TouchableOpacity
                            style={styles.btnEye}
                            onPress={this._showPassword.bind(this)}>
                            <Ionicon
                                name={
                                    this.state.press == false ? 'md-eye' : 'md-eye-off'
                                }
                                size={20}
                                color="#066d68"
                            />
                        </TouchableOpacity>
                    </View>
                    { this._displayLoading() }

                    <View style={styles.otherLogin}>
                        <TouchableOpacity style={{flex: 1}}>
                            <Text style={{textAlign: 'left', color: "#9999998f"}}>Mot de passe oublié?</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex: 1}} onPress={() => this._goToRegister()}>
                            <Text style={{textAlign: 'right', color: "#1ca7a0"}}>S'inscrire</Text>
                        </TouchableOpacity>
                    </View>
                    
                </View>
            )
        }
    }

    _showPassword = () => {
		if(this.state.press == false){
			this.setState({
				keyHide: false,
				press: true,
			});
		}else{
			this.setState({
				keyHide: true,
				press: false,
			});
		}
    };

    _goToRegister = () => {
        this.props.navigation.navigate('Register')
    }

    _login = async () => {
        this.setState({
            isLoading: true
        })
        const bodyRequest = {
            email: this.email,
            password: this.password,
        }
        let validation = await loginformvalidation(bodyRequest)
        if(validation){
            let login = await fetch(urlServer+'login', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(bodyRequest)
            }).then(res => {
                return res.json()
            }).catch(err => {
                Notification({
                    type: 'danger', 
                    message: 'Une erreur s\'est produite, veuillez vérifier votre connexion',
                    duration: 5000
                })
            })
            if(login && login.success){
                if(login.success.activated){
                    if(login.success.token){
                        this.context.login(
                            login.success.userId,
                            login.success.token
                        )
                    }
                }else{
                    Notification({
                        type: 'danger', 
                        message: 'Votre compte n\'est pas activé',
                        duration: 5000
                    })
                }
            }else{
                Notification({
                    type: 'danger', 
                    message: 'E-mail ou mot de passe incorrect',
                    duration: 5000
                })
            }
            this.setState({
                isLoading: false
            })
        }else{
            this.setState({
                isLoading: false
            })
        }
    }

    _goAfterLogin = () => {
        this.props.navigation.reset({
            index: 0,
            routes: [{ name: 'PrincipalMenu' }],
        });
    }

    _displayLoading = () => {
		if(this.state.isLoading){
			return (
				<View style={styles.btnLogin}>
                    <ActivityIndicator size="large" color="#fff" />
                </View>
			);
		}else{
			return (
				<TouchableOpacity style={styles.btnLogin} onPress={() => this._login()}>
                    <Text style={{ color: '#fff', fontSize: 18 }}>Connexion</Text>
                </TouchableOpacity>
			);
		}
	};
    
    render(){
        return(
            <AuthContext.Consumer>
                {context => {
                    if(context.token){
                        this.props.navigation.reset({
                            index: 0,
                            routes: [{name: 'PrincipalMenu'}]
                        })
                    }else{
                        return(
                            <KeyboardAvoidingView 
                                style={styles.loginContainer}
                                behavior={'height'}
                                keyboardVerticalOffset={-150}
                            >
                                <StatusBar backgroundColor="#066d68"/>
                                <View style={styles.headerLogin}>
                                    <Image 
                                        style={{width: 160, height: 100,}}
                                        source={require('../ressource/images/log.png')}
                                    />
                                </View>
                                { this._displayLoadingVerification() }
                            </KeyboardAvoidingView>
                        )
                    }
                }}
            </AuthContext.Consumer>
        )
    }
}

const styles = Styles
export default Login