import React from 'react';
import { View, Text, TextInput, FlatList, TouchableOpacity, SafeAreaView, ScrollView, } from 'react-native';
import { CheckBox } from 'react-native-elements';

import Styles from '../ressource/styles/styles';
import BasketProgressItem from '../components/basket-progress-item'

class ConfirmCommand extends React.Component{

    constructor(props) {
        super(props)
        
    }
    render(){
        const {data, totalPrice} = this.props.route.params
        return(
            <View style={styles.mainContainer}>
                <View style={styles.panelDisplay}>
                    <FlatList 
                        style={ styles.list }
                        data={ data }
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={({item, index}) => (
                            <BasketProgressItem 
                                data={item}
                                index={index}
                                confirm={true}
                            />
                        )}
                        numColumns={1}
                    />
                    <View style={styles.totalPrice}>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}>
                            <Text style={{ marginRight: 20}}>Total</Text>
                            <Text style={{ marginRight: 20, color: "#1ca7a0"}}>{ totalPrice } Ar</Text>
                        </View>
                    </View>
                    <View style={styles.btnCommandSection}>
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                            <TouchableOpacity style={styles.btnCommand}>
                                <Text style={{color: "#fff"}}>Confirmer</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = Styles
export default ConfirmCommand