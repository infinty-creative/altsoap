import React from 'react';
import { View, Text, ActivityIndicator, TouchableOpacity } from 'react-native';
import Fonticon from 'react-native-vector-icons/FontAwesome';

import Styles from '../ressource/styles/styles';
import ProfileGeneral from '../components/profile-general';
import { urlServer } from '../ressource/api/Url';
import { Notification } from '../components/layout/notification';
import { AuthContext } from '../context/auth-context';

class Profile extends React.Component{

    static contextType = AuthContext;

    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            data: null
        }
    }

    componentDidMount(){
        this._checkData()
    }

    _displayProfile = () => {
        if(this.state.data != null && this.state.data != undefined && this.state.isLoading != true){
            return(
                <ProfileGeneral 
                    data={this.state.data} 
                    navigation={this.props.navigation} 
                    updatedata={this._updateData}
                    cancelupdate={this._cancelUpdate}
                />
            )
        }else if(this.state.isLoading == false){
            return(
                <View style={{flex: 4}}>

                </View>
            )
        }
    }

    _cancelUpdate = () => {
        this._checkData()
    }

    _displayLoading = () => {
		if(this.state.isLoading){
			return(
				<View style={styles.loading_container}>
					<ActivityIndicator size="large" color="#000000"/>
				</View>
			)
		}
    }

    _updateData = (data) => {
        this.setState({
            data: data
        })
    }

    _logtout = async () => {
        await this.context.logout()
        this.props.navigation.reset({
            index: 0,
            routes: [{name: 'Login'}]
        })
    }

    _checkData = async () => {
        this.setState({
            isLoading: true
        })
        let data = await fetch(urlServer+'user/'+this.context.idUser, {
            method: 'GET',
            headers: {
                Authorization: 'Bearer '+this.context.token
            }
        }).then(res => {
            return res.json()
        }).catch(err => {
            Notification({
                type: 'danger', 
                message: 'Une erreur s\'est produite, veuillez vérifier votre connexion',
                duration: 5000
            })
        })
        if(data){
            this.setState({
                isLoading: false,
                data: data.data[0]
            })
        }else{
            this.setState({
                isLoading: false
            })
        }
    }

    render(){
        return(
            <View style={styles.mainContainer}>
                <View style={styles.head}>
                    <Text style={styles.headText}>Général</Text>
                    <Fonticon 
                        style={styles.refresh}
                        name="refresh" 
                        size={17} 
                        color="#191a19"
                        onPress={() => this._checkData()}
                    />
                </View>
                { this._displayLoading() }
                { this._displayProfile() }
                <TouchableOpacity 
                    style={{
                        height: 30,
                        backgroundColor: 'red',
                        marginBottom: 10,
                        alignItems: 'center', 
                        justifyContent: 'center'
                    }}
                    onPress={() => this._logtout()}
                >
                    <Text style={{textAlign: 'center', color: '#fff'}}>
                        Deconnexion
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = Styles
export default Profile