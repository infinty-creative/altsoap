import React from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';

import Styles from '../ressource/styles/styles';
import ProductDetail from '../components/product-detail'
import { urlServer } from '../ressource/api/Url';
import { Notification } from '../components/layout/notification';

class ProductDetails extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            quantity: 0
        }
    }

    _add = () => {
        this.setState({
            quantity: this.state.quantity += 1
        })
    }
    _remove = () => {
        if(this.state.quantity > 0){
            this.setState({
                quantity: this.state.quantity -= 1
            })
        }
    }

    render(){
        const { data } = this.props.route.params
        return(
            <View style={{flex:1}}>
                <View style={styles.mainContainer}>
                    <ProductDetail data={data}/>
                </View>
                <View style={styles.actionSell}>
                    <View style={styles.actionSection}>
                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <Text style={{color: "#ffffff"}}>QTE</Text>
                            <TouchableOpacity style={[styles.btnAdd,{marginLeft: 20}]} onPress={() => this._remove()}>
                                <Text style={{color: "#ffffff", textAlign: "center"}}>-</Text>
                            </TouchableOpacity>
                            <TextInput 
                                value={this.state.quantity.toString()}
                                style={{
                                    backgroundColor: '#fff',
                                    borderRadius: 15,
                                    marginLeft: 5,
                                    marginRight: 5,
                                    width: 40,
                                    color: '#000',
                                    fontSize: 15,
                                    paddingTop: 0,
                                    paddingBottom: 0,
                                    textAlign: 'center'
                                }}
                            />
                            <TouchableOpacity style={styles.btnAdd} onPress={() => this._add()}>
                                <Text style={{color: "#ffffff", textAlign: "center"}}>+</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 1, alignItems:'flex-end'}}>
                            <TouchableOpacity style={styles.btnBuy}>
                                <Text style={{color: "#ffffff", textAlign: "center"}}>Ajouter au panier</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = Styles
export default ProductDetails