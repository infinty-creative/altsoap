import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import Fonticon from 'react-native-vector-icons/FontAwesome';

import Styles from '../ressource/styles/styles';
import BasketProgressList from '../components/basket-progress';

class BasketProgress extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            check: false,
            data: null
        }
    }

    componentDidMount(){
        this._checkData()
    }

    _displayData = () => {
        if(this.state.data != null && this.state.data != undefined && this.state.isLoading != true){
            return(
                <BasketProgressList 
                    data={this.state.data} 
                    navigation={this.props.navigation} 
                    checkdata={this._checkData} 
                    loading={this.state.isLoading}
                />
            )
        }    
    }

    _displayLoading = () => {
		if(this.state.isLoading){
			return(
				<View style={styles.loading_container}>
					<ActivityIndicator size="large" color="#000000"/>
				</View>
			)
		}
    }

    _checkData = () => {
        this.setState({
            isLoading: true,
            data: null
        }, () => {
            let data = liste
            if(data){
                this.setState({
                    isLoading: false,
                    data: data
                })
            }else{
                this.setState({
                    isLoading: false,
                })
            }
        })
    }

    render(){
        return(
            <View style={styles.mainContainer}>
                <View style={styles.head}>
                    <Text style={styles.headText}>Mon panier</Text>
                    <Fonticon 
                        style={styles.refresh}
                        name="refresh" 
                        size={17} 
                        color="#191a19"
                        onPress={() => this._checkData()}
                    />
                </View>
                { this._displayLoading() }
                { this._displayData() }
            </View>
        )
    }
}
const liste = 
    {
        data: [
            {
                id: '1',
                nom: 'Green Soap',
                image: 'https://www.afrik21.africa/wp-content/uploads/2020/04/92245408_10223078516610507_8386932125235937280_n-720x400.jpg',
                description: 'lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum',
                prix: '300',
                quantity: 4
        
            },
            {
                id: '2',
                id: '2',
                nom: 'Brown Soap',
                image: 'https://img.lemde.fr/2020/06/18/1188/0/3688/1840/1440/720/60/0/2bee475_GFmeOmvGBb4qrqztvSjB1a5D.jpg',
                description: 'lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum',
                prix: '400',
                quantity: 5
        
            },
        ],
        totalPrice: '8000'
    }

const styles = Styles
export default BasketProgress