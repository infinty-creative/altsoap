import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import Fonticon from 'react-native-vector-icons/FontAwesome';

import Styles from '../ressource/styles/styles';
import PurchaseList from '../components/purchase-list'

class ProfilePurchase extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            listPurchase: null
        }
    }

    componentDidMount(){
        this._checkData()
    }

    _displayList = () => {
        if(this.state.listPurchase != null && this.state.listPurchase != undefined && this.state.isLoading != true)
        return(
            <PurchaseList liste={this.state.listPurchase}/>
        )
    }

    _displayDetail = (id) => {
        this.props.navigation.navigate('PurchaseDetail', {id: id})
    }

    _displayLoading = () => {
		if(this.state.isLoading){
			return(
				<View style={styles.loading_container}>
					<ActivityIndicator size="large" color="#000000"/>
				</View>
			)
		}
    }
    
    _checkData = () => {
        this.setState({
            isLoading: true
        })
        const data = liste
        if(data){
            this.setState({
                isLoading: false,
                listPurchase: data
            })
        }else{
            this.setState({
                isLoading: false
            })
        }
    }

    render(){
        return(
            <View style={styles.mainContainer}>
                <View style={styles.head}>
                    <Text style={styles.headText}>Achats éfféctuées</Text>
                    <Fonticon 
                        style={styles.refresh}
                        name="refresh" 
                        size={17} 
                        color="#191a19"
                        onPress={() => this._checkData()}
                    />
                </View>
                { this._displayLoading() }
                { this._displayList() }
            </View>
        )
    }
}


const liste = [
    {   
        id: '1',
        date: '01/06/2020',
        heure: '8:30'
    },
    {   
        id: '2',
        date: '02/06/2020',
        heure: '9:30'
    },
    {   
        id: '3',
        date: '03/06/2020',
        heure: '10:30'
    },
    {
        id: '4',
        date: '04/06/2020',
        heure: '11:30'
    }
]

const styles = Styles
export default ProfilePurchase