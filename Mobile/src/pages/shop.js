import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import Fonticon from 'react-native-vector-icons/FontAwesome';

import { AuthContext } from '../context/auth-context';
import { urlServer } from '../ressource/api/Url';
import { Notification } from '../components/layout/notification';
import Styles from '../ressource/styles/styles';
import ProductList from '../components/product-list';

class Shop extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            data: null
        }
    }

    componentDidMount(){
        this._checkData()
    }

    _displayData = () => {
        if(this.state.data != null && this.state.data != undefined && this.state.isLoading != true){
            return(
                <ProductList data={this.state.data} navigation={this.props.navigation}/>
            )
        }
        
    }

    _displayLoading = () => {
		if(this.state.isLoading){
			return(
				<View style={styles.loading_container}>
					<ActivityIndicator size="large" color="#000000"/>
				</View>
			)
		}
    }

    _checkData = async () => {
        this.setState({
            isLoading: true
        })
        let data = await fetch(urlServer+'categories/1/products/', {
            method: 'GET',
        }).then(res => {
            return res.json()
        }).catch(err => {
            Notification({
                type: 'danger', 
                message: 'Une erreur s\'est produite, veuillez vérifier votre connexion et rafraichir la page',
                duration: 5000
            })
        })
        if(data.data){
            this.setState({
                isLoading: false,
                data: data.data
            })
        }else{
            this.setState({
                isLoading: false
            })
        }
    }

    render(){
        return(
            <View style={styles.mainContainer}>
                <View style={styles.head}>
                    <Text style={styles.headText}>Nos produits</Text>
                    <Fonticon 
                        style={styles.refresh}
                        name="refresh" 
                        size={17} 
                        color="#191a19"
                        onPress={() => this._checkData()}
                    />
                </View>
                { this._displayLoading() }
                { this._displayData() }
            </View>
        )
    }
}
const styles = Styles
export default Shop