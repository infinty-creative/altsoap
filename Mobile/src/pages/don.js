import React from 'react';
import { View, Text, TextInput, KeyboardAvoidingView, TouchableOpacity, SafeAreaView, ScrollView, } from 'react-native';
import { CheckBox } from 'react-native-elements';

import Styles from '../ressource/styles/styles';

class Don extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            security: null,
            checked: false
        }
        this.adress = null;
        this.phone = null;
        this.quantity = null;
        this.country = null;
    }
    render(){
        return(
            <ScrollView style={styles.mainContainer}>
                <View style={styles.head}>
                    <Text style={styles.headText}>Faire un don d'huile</Text>
                </View>
                <SafeAreaView style={styles.panelDisplay}>
                <KeyboardAvoidingView 
                    behavior={'height'}
                >
                    <Text style={{color: "#535353"}}>Adresse</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            value={this.adress}
                            placeholderTextColor={'#535353'}
                            underlineColorAndroid="transparent"
                            style={styles.textInput}
                            onChangeText={value => (this.adress = value)}
                        />
                    </View>
                    <Text style={{color: "#535353"}}>Téléphone</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            value={this.phone}
                            placeholderTextColor={'#535353'}
                            underlineColorAndroid="transparent"
                            style={styles.textInput}
                            onChangeText={value => (this.phone = value)}
                        />
                    </View>
                    <Text style={{color: "#535353"}}>Quantité</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            value={this.quantity}
                            placeholderTextColor={'#535353'}
                            underlineColorAndroid="transparent"
                            style={styles.textInput}
                            onChangeText={value => (this.quantity = value)}
                        />
                    </View>
                    <Text style={{color: "#535353"}}>Pays</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            value={this.country}
                            placeholderTextColor={'#535353'}
                            underlineColorAndroid="transparent"
                            style={styles.textInput}
                            onChangeText={value => (this.country = value)}
                        />
                    </View>

                    <CheckBox
                        center
                        title='Avec récipient'
                        iconRight
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checkedColor='#1ca7a0'
                        checked={this.state.checked}
                        onPress={() => this.setState({checked: !this.state.checked})}
                        textStyle={{
                            fontWeight: 'normal',
                            color: '#000000'
                        }}
                        containerStyle={{
                            marginTop: 20,
                            height: 40,
                            marginLeft: -10,
                            marginRight: -10
                        }}
                    />
                    <TouchableOpacity style={styles.btnUpdateOrange}>
                        <Text style={{color: "#ffffff"}}>Valider</Text>
                    </TouchableOpacity>
                </KeyboardAvoidingView>
                </SafeAreaView>
            </ScrollView>
        )
    }
}

const styles = Styles
export default Don