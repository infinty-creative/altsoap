import React from 'react';
import { Text, TouchableOpacity, Image } from 'react-native';
import { Card} from 'react-native-elements';

import Styles from '../ressource/styles/styles'

class ProductListItem extends React.Component{

	constructor(props){
		super(props)
	}

	render(){
        const { data, index, showDetail } = this.props
		return(
			<TouchableOpacity style={ [styles.listItemShop, { marginBottom: 10 }] } onPress={() => showDetail(data)}>
                <Card
                    image={{uri: data.image}}
                >
                    <Text style={{marginBottom: 10, color: "#1ca7a0", textAlign: "center"}}>
                        { data.nameProduct }
                        
                    </Text>
                    <Text style={{marginBottom: 10, color: "#000000", textAlign: "center"}}>
                        { data.price } Ar
                        
                    </Text>

                </Card>
            </TouchableOpacity>
		)
	}

}

const styles = Styles

export default ProductListItem