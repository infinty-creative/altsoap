import React from 'react';
import { Text, View, TouchableOpacity, Image, TextInput } from 'react-native';
import Fonticon from 'react-native-vector-icons/FontAwesome';

import Styles from '../ressource/styles/styles'

class BasketProgressItem extends React.Component{

	constructor(props){
        super(props)
        this.data = this.props.data
        this.removeBasket = this.props.data
	}

	render(){
        const { data, removeBasket, addquantity, dropquantity, checkdata, index, confirm } = this.props
		return(
			<View style={ styles.listItemBasket }>
                <View style={{flex:1, flexDirection: 'row'}} >
                    <View style={styles.imageContainer}>
                        <Image 
                            style={styles.imageBracketList}
                            source={
                                {uri: data.image}
                            }
                        />
                    </View>
                    <View style={styles.productInfo}>
                        <Text style={{marginBottom: 10}}>{data.nom}</Text>
                        <Text style={{color: "#ec7e49", marginBottom: 10}}>{data.prix} Ar</Text>
                    </View>
                </View>
                <View style={{flex: 0.5, alignItems: 'center'}}>
                    <Text>QTE</Text>
                    {
                        !confirm ?
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                <TouchableOpacity style={[styles.btnAdd]} onPress={() => dropquantity(index)}>
                                    <Text style={{color: "#ffffff", textAlign: "center"}}>-</Text>
                                </TouchableOpacity>
                                <TextInput 
                                    value={data.quantity.toString()}
                                    style={{
                                        backgroundColor: '#9999998f',
                                        borderRadius: 15,
                                        marginLeft: 5,
                                        marginRight: 5,
                                        width: 40,
                                        height: 20,
                                        color: '#000',
                                        fontSize: 15,
                                        paddingTop: 0,
                                        paddingBottom: 0,
                                        textAlign: 'center'
                                    }}
                                />
                                <TouchableOpacity style={styles.btnAdd} onPress={() => addquantity(index)}>
                                    <Text style={{color: "#ffffff", textAlign: "center"}}>+</Text>
                                </TouchableOpacity>
                            </View>
                                :
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                <Text
                                    style={{
                                        marginLeft: 5,
                                        marginRight: 5,
                                        width: 70,
                                        height: 50,
                                        color: '#ec7e49',
                                        fontSize: 30,
                                        paddingTop: 0,
                                        paddingBottom: 0,
                                        textAlign: 'center'
                                    }}
                                >
                                    { data.quantity }
                                </Text>
                            </View>
                    }
                </View>
                {
                    !confirm &&
                    <Fonticon 
                        name="remove"
                        size={25}
                        color="black"
                        style={{
                            position: "absolute",
                            top: 5,
                            left: 10
                        }}
                        onPress={() => removeBasket(data.id)}
                    />
                }
                
            </View>
		)
	}

}

const styles = Styles
export default BasketProgressItem