import React from 'react';
import { 
    View, 
    Text, 
    TextInput, 
    KeyboardAvoidingView, 
    TouchableOpacity, 
    ScrollView, 
    SafeAreaView,
    // Picker,
    ActivityIndicator
} from 'react-native';
import { Picker } from '@react-native-community/picker';
import countryList from 'react-select-country-list';

import Styles from '../ressource/styles/styles';
import { AuthContext } from '../context/auth-context';
import { urlServer } from '../ressource/api/Url';
import { Notification } from '../components/layout/notification';
import { generalformvalidation } from '../components/layout/form-validation';

class ProfileGeneral extends React.Component{

    static contextType = AuthContext;

    constructor(props) {
        super(props)
        this.state = {
            updating: false,
            data: this.props.data,
            country: countryList().getData(),
            isLoading: false
        }
        this.name = user.name;
        this.lastname = user.lastname;
        this.email = user.email;
        this.phone = user.phone;
        this.country = user.country;
        this.adress = user.adress;
    }

    _updateData = async() => {
        this.setState({
            isLoading: true
        })
        const bodyRequest = {...this.state.data}
        let validation = generalformvalidation(bodyRequest)
        if(validation){
            let datas = await fetch(urlServer+'user/'+this.context.idUser+'', {
                method: 'PUT',
                headers: {
                    Authorization: 'Bearer '+this.context.token,
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(bodyRequest)
            }).then(res => {
                return res.json()
            }).catch(err => {
                Notification({
                    type: 'danger', 
                    message: 'Une erreur s\'est produite, veuillez vérifier votre connexion et réessayer',
                    duration: 5000
                })
            })
            this.props.updatedata(datas)
            this.setState({
                isLoading: false,
                updating: !this.state.updating
            })
            Notification({
                type: 'success', 
                message: 'Modification réussie',
                duration: 5000
            })
        }else{
            this.setState({
                isLoading: false,
                updating: !this.state.updating
            })
        }
        
    }

    _updateMode = () => {
        if(this.state.updating){
            if(this.state.isLoading){
                return(
                    <View 
                        style={styles.btnUpdateOrange}
                    >
                        <ActivityIndicator size="large" color="#fff" />
                    </View>
                )
            }else{
                return(
                    <View style={{
                        flex: 1, 
                        flexDirection: 'row', 
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <TouchableOpacity 
                            style={styles.btnUpdateVert}
                            onPress={() => this._updateData()}
                        >
                            <Text style={{color: "#ffffff"}}>Sauvegarder</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={styles.btnUpdateRed}
                            onPress={() => this.props.cancelupdate()}
                        >
                            <Text style={{color: "#ffffff"}}>Annuler</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
        }else{
            return(
                <TouchableOpacity 
                    style={styles.btnUpdateOrange}
                    onPress={() => {
                        this.setState({
                            updating: !this.state.updating
                        })
                    }}
                >
                    <Text style={{color: "#ffffff"}}>Modifier</Text>
                </TouchableOpacity>
            )
        }
    }

    render(){
        const {navigation} =  this.props
        return(
            <SafeAreaView style={styles.panelDisplay}>
                <ScrollView>
                    <KeyboardAvoidingView 
                        behavior={'height'}
                    >
                        <Text style={{color: "#535353"}}>Nom</Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                value={this.state.data.name}
                                placeholderTextColor={'#535353'}
                                underlineColorAndroid="transparent"
                                style={styles.textInput}
                                onChangeText={value => {
                                    let data = this.state.data
                                    data.name = value
                                    this.setState({
                                        data: data
                                    }) 
                                }}
                                editable={this.state.updating}
                            />
                        </View>
                        <Text style={{color: "#535353"}}>Prénom</Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                value={this.state.data.lastname}
                                placeholderTextColor={'#535353'}
                                underlineColorAndroid="transparent"
                                style={styles.textInput}
                                onChangeText={value => {
                                    let data = this.state.data
                                    data.lastname = value
                                    this.setState({
                                        data: data
                                    }) 
                                }}
                                editable={this.state.updating}
                            />
                        </View>
                        <Text style={{color: "#535353"}}>E-mail</Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                value={this.state.data.email}
                                placeholderTextColor={'#535353'}
                                underlineColorAndroid="transparent"
                                style={styles.textInput}
                                onChangeText={value => {
                                    let data = this.state.data
                                    data.email = value
                                    this.setState({
                                        data: data
                                    }) 
                                }}
                                editable={false}
                            />
                        </View>
                        <Text style={{color: "#535353"}}>Téléphone</Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                value={this.state.data.phone}
                                placeholderTextColor={'#535353'}
                                underlineColorAndroid="transparent"
                                style={styles.textInput}
                                onChangeText={value => {
                                    let data = this.state.data
                                    data.phone = value
                                    this.setState({
                                        data: data
                                    }) 
                                }}
                                editable={this.state.updating}
                            />
                        </View>
                        <Text style={{color: "#535353"}}>Pays</Text>
                        <View style={styles.inputContainer}>
                            <View style={styles.textInput}>
                                <Picker
                                    selectedValue={this.state.data.country}
                                    onValueChange={(itemValue, itemIndex) => {
                                        let data = this.state.data
                                        data.country = itemValue
                                        this.setState({
                                            data: data
                                        }) 
                                    }}
                                    enabled={this.state.updating}
                                >
                                    { this.state.country.map((item, key = 0) => {
                                        key += 1
                                        return(
                                            <Picker.Item 
                                                label={item.label} 
                                                value={item.label} 
                                                key={key}
                                            />
                                        )
                                    }) }
                                    <Picker.Item label="Java" value="java" />
                                    <Picker.Item label="JavaScript" value="js" />
                                </Picker>
                            </View>
                        </View>
                        <Text style={{color: "#535353"}}>Adresse</Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                value={this.state.data.address}
                                placeholderTextColor={'#535353'}
                                underlineColorAndroid="transparent"
                                style={styles.textInput}
                                onChangeText={value => (this.adress = value)}
                                editable={false}
                            />
                        </View>
                        { this._updateMode() }
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const user = {
    name: 'Gisenaël',
    lastname: 'Michel',
    email: 'gisenael.dev44@gmail.com',
    phone: '0340000000',
    country: 'Madagascar',
    adress: 'Tananarive Analakely'
}
const styles = Styles
export default ProfileGeneral