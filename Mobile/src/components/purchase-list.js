import React from 'react';
import { View, FlatList } from 'react-native';
import Styles from '../ressource/styles/styles'
import PurchaseListItem from './purchase-list-item'

class PurchaseList extends React.Component{

    constructor(props) {
	    super(props)
    }

    render(){
        const {liste} =  this.props
        return(
            <View style={styles.panelDisplay}>
                <FlatList
                    style={ styles.list }
                    data={ liste }
                    // keyExtractor={(item) => }
                    renderItem={({item, index}) => (
                            <PurchaseListItem 
                                purchase={item}
                                index={index}
                            />
                    )} 
                >
                </FlatList>
            </View>
        )
    }
}

const styles = Styles
export default PurchaseList