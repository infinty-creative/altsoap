import { Notification } from './notification';

export async function loginformvalidation(value){
    if(value.email != null && value.email.length > 0 && value.password != null && value.password.length > 0){
        if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value.email)){
            return true
        }else{
            const notif = {
                type: 'danger',
                message: 'E-mail invalide',
                duration: 5000
            }
            Notification(notif)
            return false
        }
    }else{
        const notif = {
            type: 'danger',
            message: 'Veuillez remplir les champs',
            duration: 5000
        }
        Notification(notif)
        return false
    }
}

export async function registerformvalidation(value){
    if(
        value.name != null 
        && value.name.length > 0
        && value.lastname != null 
        && value.lastname.length > 0
        && value.phone != null 
        && value.phone.length > 0
        && value.email != null 
        && value.email.length > 0
        && value.country != null 
        && value.country.length > 0
        && value.password != null 
        && value.password.length > 0
        && value.address != null 
        && value.address.length > 0
        && value.c_password != null 
        && value.c_password.length > 0
    ){
        if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value.email)){
            if(parseInt(value.phone) != NaN && parseInt(value.phone) != undefined && value.phone.length > 8){
                if(value.password.length > 7){
                    if(value.password === value.c_password){
                        return true
                    }else{
                        const notif = {
                            type: 'danger',
                            message: 'Veuillez confirmer votre mot de passe',
                            duration: 5000
                        }
                        Notification(notif)
                        return false
                    }
                }else{
                    const notif = {
                        type: 'danger',
                        message: 'Mot de passe 8 charactère minimum',
                        duration: 5000
                    }
                    Notification(notif)
                    return false
                }
            }else{
                const notif = {
                    type: 'danger',
                    message: 'Format numéro téléphone invalide',
                    duration: 5000
                }
                Notification(notif)
                return false
            }
        }else{
            const notif = {
                type: 'danger',
                message: 'E-mail invalide',
                duration: 5000
            }
            Notification(notif)
            return false
        }
    }else{
        const notif = {
            type: 'danger',
            message: 'Veuillez remplir tous les champs',
            duration: 5000
        }
        Notification(notif)
        return false
    }
}

export async function generalformvalidation(value){
    if(
        value.name != null 
        && value.name.length > 0
        && value.lastname != null 
        && value.lastname.length > 0
        && value.phone != null 
        && value.phone.length > 0
        && value.country != null 
        && value.country.length > 0
        && value.address != null 
        && value.address.length > 0
    ){
        
        if(parseInt(value.phone) != NaN && parseInt(value.phone) != undefined && value.phone.length > 8){
           return true
        }else{
            const notif = {
                type: 'danger',
                message: 'Format numéro téléphone invalide',
                duration: 5000
            }
            Notification(notif)
            return false
        }
        
    }else{
        const notif = {
            type: 'danger',
            message: 'Veuillez remplir tous les champs',
            duration: 5000
        }
        Notification(notif)
        return false
    }
}

export async function passwordformvalidation(value){
    if(
        value.oldpassword != null 
        && value.oldpassword.length > 0
        && value.newpassword != null 
        && value.newpassword.length > 0
        && value.passconfirm != null 
        && value.passconfirm.length > 0
    ){
        if(value.newpassword.length < 8 && value.passconfirm.length < 8){
            const notif = {
                type: 'danger',
                message: 'Mot de passe 8 charactère minimum',
                duration: 5000
            }
            Notification(notif)
            return false
        }else{
            if(value.oldpassword === value.newpassword){
                const notif = {
                    type: 'danger',
                    message: 'Votre nouveau mot de passe est le même que l\'ancien',
                    duration: 5000
                }
                Notification(notif)
                return false
            }else{
                if(value.newpassword === value.passconfirm){
                    return true
                }else{
                    const notif = {
                        type: 'danger',
                        message: 'Veuillez confirmer votre mot de passe',
                        duration: 5000
                    }
                    Notification(notif)
                    return false
                }
            }
        }
    }else{
        const notif = {
            type: 'danger',
            message: 'Veuillez remplir tous les champs',
            duration: 8000
        }
        Notification(notif)
        return false
    }
}