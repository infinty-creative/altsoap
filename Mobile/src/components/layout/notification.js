import React from 'react-native'
import { showMessage, hideMessage } from "react-native-flash-message";

export function Notification(value){
    if(value != null && value != undefined){
        let bgColor
        if(value.type == 'success'){
            bgColor = '#39e716'
        }else if(value.type == 'danger'){
            bgColor = 'red'
        }else{
            bgColor = null
        }
		showMessage({
            message: value.message,
            type: value.type,
            autoHide: true,
            duration: value.duration,
            backgroundColor: bgColor,
            onPress: () => {
                hideMessage()
            }
        })
	}
}