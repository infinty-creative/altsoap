import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';

import Styles from '../ressource/styles/styles';
import BasketProgressItem from '../components/basket-progress-item'

class BasketProgressList extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            data: null,
            total: null,
            update: false
        }
    }

    componentDidMount(){
        this.setState({
            data: this.props.data.data,
            total: this.props.data.totalPrice
        })
    }

    _removeBasket = (id) => {
        console.log(id)
    }

    _addQuantity = (i) => {
        let dataTmp = this.state.data
        dataTmp[i].quantity += 1
        this.setState({
            data: dataTmp,
            update: true
        })
        
    }

    _confirmcommand = () => {
        this.props.navigation.navigate('ConfirmCommand', {data: this.state.data, totalPrice: this.state.total})
    }

    _showUpdateButton = () => {
        if(this.state.update == true){
            return(
                <TouchableOpacity style={styles.btnUpdateCommand}>
                    <Text style={{color: "#fff"}}>Mettre à jour</Text>
                </TouchableOpacity>
            )
        }
    }

    _dropQuantity = (i) => {
        let dataTmp = this.state.data
        if(dataTmp[i].quantity > 0){
            dataTmp[i].quantity -= 1
            this.setState({
                data: dataTmp,
                update: true
            })
        }
    }

    render(){
        const {checkdata} = this.props

        return(
            <View style={styles.panelDisplay}>
                <FlatList 
                    style={ styles.list }
                    data={ this.state.data }
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({item, index}) => (
                        <BasketProgressItem 
                            data={item}
                            index={index}
                            removeBasket={this._removeBasket}
                            addquantity={this._addQuantity}
                            dropquantity={this._dropQuantity}
                            checkdata = {checkdata}
                        />
                    )}
                    numColumns={1}
                />
                <View style={styles.totalPrice}>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}>
                        <Text style={{ marginRight: 20}}>Total</Text>
                        <Text style={{ marginRight: 20, color: "#1ca7a0"}}>{ this.state.total } Ar</Text>
                    </View>
                </View>
                <View style={styles.btnCommandSection}>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <TouchableOpacity style={styles.btnCommand} onPress={() => this._confirmcommand()}>
                            <Text style={{color: "#fff"}}>Commander</Text>
                        </TouchableOpacity>
                        { this._showUpdateButton() }
                    </View>
                </View>
            </View>
        )
    }
}

const styles = Styles
export default BasketProgressList