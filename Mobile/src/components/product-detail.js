import React from 'react';
import { View, Text, Image, ScrollView } from 'react-native';

import Styles from '../ressource/styles/styles'

class ProductDetail extends React.Component{

    constructor(props) {
        super(props)
        
    }

    
    render(){
        const { data } = this.props
        return(
            <View style={styles.scrollview_container}>
                <Image 
                    style={styles.imageDetail}
                    source={
                        {uri: data.image}
                    }
                />
                <View style={{flex: 0.5}}>
                    <View style={styles.title_container}>
                        <Text style={styles.title}>{data.nameProduct.toUpperCase()}</Text>
                        <Text style={styles.prix}>{data.price} Ar</Text>
                    </View>
                    <ScrollView style={styles.description_container}>
                        <Text style={styles.description}>{data.description}</Text>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = Styles
export default ProductDetail