import React from 'react';
import { View, Text, TextInput, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';

import Styles from '../ressource/styles/styles'

class ProfilePassword extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            updating: false,
            oldpass: null,
            keyHide: true,
            press: false,
        }
        this.oldpass = null;
        this.newpass = null;
        this.passconfirm = null;

    }

    _showPassword = () => {
		if(this.state.press == false){
			this.setState({
                keyHide: false,
				press: true,
			});
		}else{
			this.setState({
				keyHide: true,
				press: false,
			});
		}
	};

    render(){
        const {data} =  this.props
        return(
            <KeyboardAvoidingView 
                style={styles.panelDisplay}
            >
                <Text style={{color: "#535353"}}>Ancien mot de passe</Text>
                <View style={styles.inputContainer}>
                    <TextInput
                        placeholder="••••"
                        placeholderTextColor={'#535353'}
                        underlineColorAndroid="transparent"
                        secureTextEntry={this.state.keyHide}
                        style={styles.textInput}
                        onChangeText={value => (this.oldpass = value)}
                    />
                    <TouchableOpacity
                        style={styles.btnEye}
                        onPress={this._showPassword.bind(this)}>
                        <Ionicon
                            name={
                                this.state.press == false ? 'md-eye' : 'md-eye-off'
                            }
                            size={20}
                            color="#ffffff"
                        />
                    </TouchableOpacity>
                </View>
                <Text style={{color: "#535353"}}>Nouveau mot de passe</Text>
                <View style={styles.inputContainer}>
                    <TextInput
                        placeholder="••••"
                        placeholderTextColor={'#535353'}
                        underlineColorAndroid="transparent"
                        secureTextEntry={this.state.keyHide}
                        style={styles.textInput}
                        onChangeText={value => (this.newpass = value)}
                    />
                    <TouchableOpacity
                        style={styles.btnEye}
                        onPress={this._showPassword.bind(this)}>
                        <Ionicon
                            name={
                                this.state.press == false ? 'md-eye' : 'md-eye-off'
                            }
                            size={20}
                            color="#ffffff"
                        />
                    </TouchableOpacity>
                </View>
                <Text style={{color: "#535353"}}>Confirmer mot de passe</Text>
                <View style={styles.inputContainer}>
                    <TextInput
                        placeholder="••••"
                        placeholderTextColor={'#535353'}
                        underlineColorAndroid="transparent"
                        secureTextEntry={this.state.keyHide}
                        style={styles.textInput}
                        onChangeText={value => (this.passconfirm = value)}
                    />
                    <TouchableOpacity
                        style={styles.btnEye}
                        onPress={this._showPassword.bind(this)}>
                        <Ionicon
                            name={
                                this.state.press == false ? 'md-eye' : 'md-eye-off'
                            }
                            size={20}
                            color="#ffffff"
                        />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.btnUpdate}>
                    <Text style={{color: "#ffffff"}}>Modifier</Text>
                </TouchableOpacity>
            </KeyboardAvoidingView>
        )
    }
}

const styles = Styles
export default ProfilePassword