import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Fonticon from 'react-native-vector-icons/FontAwesome';

import Styles from '../ressource/styles/styles'

class DonListItem extends React.Component{

	constructor(props){
		super(props)
	}

	render(){
        const { don, index } = this.props
        let color
        if((index % 2 == 0)){
            color = "#9999998f"
        }else{
            color = "#ffffff"
        }
		return(
			<TouchableOpacity style={ [styles.listItem, { backgroundColor: color }] }>
                <Text style={ styles.listItemDate }>{ don.date }</Text>
                <Text style={ styles.listItemHeure }>{ don.heure }</Text>
                <View style={ styles.listCheckDetail }>
                    <Fonticon 
                        name="search" 
                        size={17} 
                        color="#191a19"
                    />
                    
                </View>
            </TouchableOpacity>
		)
	}

}

const styles = Styles

export default DonListItem