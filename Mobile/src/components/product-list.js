import React from 'react';
import { View, FlatList} from 'react-native';

import Styles from '../ressource/styles/styles'
import ProductListItem from './product-list-item'

class ProductList extends React.Component{

    constructor(props) {
        super(props)
        // this.state = {
        //     data: this.props.data
        // }
        
    }

    _showDetail = (data) => {
        this.props.navigation.navigate('ProductDetails', {data: data})
    }

    render(){
        return(
            <View style={styles.panelDisplay}>
                <FlatList 
                    style={ styles.list }
                    data={ this.props.data }
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({item, index}) => (
                        <ProductListItem 
                            data={item}
                            index={index}
                            showDetail={this._showDetail}
                        />
                    )}
                    numColumns={2}
                />
            </View>
        )
    }
}

const styles = Styles
export default ProductList