import React from 'react';
import { View, FlatList } from 'react-native';
import Styles from '../ressource/styles/styles'
import DonListItem from './purchase-list-item'

class DonLists extends React.Component{

    constructor(props) {
	    super(props)
    }

    render(){
        const {liste} =  this.props
        return(
            <View style={styles.panelDisplay}>
                <FlatList
                    style={ styles.list }
                    data={ liste }
                    // keyExtractor={(item) => }
                    renderItem={({item, index}) => (
                            <DonListItem 
                                don={item}
                                index={index}
                            />
                    )} 
                >
                </FlatList>
            </View>
        )
    }
}

const styles = Styles
export default DonLists