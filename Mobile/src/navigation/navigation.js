import React from 'react';
import { StatusBar, Image, Text, View } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Fonticon from 'react-native-vector-icons/FontAwesome';
import Feathicon from 'react-native-vector-icons/Feather';
import Font5icon from 'react-native-vector-icons/FontAwesome5';

import Styles from '../ressource/styles/styles';
import Shop from '../pages/shop';
import BasketProgress from '../pages/basket-progress';
import Don from '../pages/don';
import Profile from '../pages/profile';
import ProfilePurchase from '../pages/profile-purchase';
import ProfileSecurity from '../pages/profile-security';
import About from '../pages/about';
import Login from '../pages/login';
import Register from '../pages/register';
import PurchaseDetail from '../pages/purchase-detail';
import ProductDetails from '../pages/product-details';
import DonList from '../pages/don-list';
import ForgotPassword from '../pages/forgot-password-verify';
import ConfirmCommand from '../pages/confirm-command';

import { AuthContext } from '../context/auth-context';

class Navigation extends React.Component{

    static contextType = AuthContext;

    constructor(props) {
        super(props)
        enableScreens();
    }

    render(){
        return(
            <NavigationContainer>
                <StatusBar backgroundColor="#1ca7a0"/>
                <StackNavigator />
            </NavigationContainer>
        )
    }
    
}

/* declartation create navigatior */
const btmTab = createBottomTabNavigator();
const stackNative = createStackNavigator();
const topSubMenuProfile = createMaterialTopTabNavigator();
const topSubMenuDon = createMaterialTopTabNavigator();

/* declaration stack navigator for each page */
const stackShop = createNativeStackNavigator();
const stackProfileGeneral = createNativeStackNavigator();
const stackProfileSecurity = createNativeStackNavigator();
const stackProfilePurchase = createNativeStackNavigator();
const stackBasketProgress = createNativeStackNavigator();
const stackDonForm = createNativeStackNavigator();
const stackDonList = createNativeStackNavigator();
const stackAbout = createNativeStackNavigator();

/* stack navigator */

const shopstack = () => {
    return(
        <stackShop.Navigator
            initialRouteName="Shop"
            screenOptions={{
                headerShown: false,
                headerHideShadow: true,
                headerStyle: {
                    backgroundColor: "#1ca7a0"
                }
            }}
        >
            <stackShop.Screen 
                name="Shop"
                component={Shop}
            />
        </stackShop.Navigator>
    )
}

const generalprofilestack = () => {
    return(
        <stackProfileGeneral.Navigator
            initialRouteName="ProfileGenerale"
            screenOptions={{
                headerShown: false,
                headerHideShadow: true,
                headerStyle: {
                    backgroundColor: "#1ca7a0"
                }
            }}
        >
            <stackProfileGeneral.Screen 
                name="ProfileGenerale"
                component={Profile}
            />
        </stackProfileGeneral.Navigator>
    )
}

const securityprofilestack = () => {
    return(
        <stackProfileSecurity.Navigator
            initialRouteName="ProfileSecurity"
            screenOptions={{
                headerShown: false,
                headerHideShadow: true,
                headerStyle: {
                    backgroundColor: "#1ca7a0"
                }
            }}
        >
            <stackProfileSecurity.Screen 
                name="ProfileSecurity"
                component={ProfileSecurity}
            />
        </stackProfileSecurity.Navigator>
    )
}

const purchaseprofilestack = () => {
    return(
        <stackProfilePurchase.Navigator
            initialRouteName="ProfilePurchase"
            screenOptions={{
                headerShown: false,
                headerHideShadow: true,
                headerStyle: {
                    backgroundColor: "#1ca7a0"
                }
            }}
        >
            <stackProfilePurchase.Screen 
                name="ProfilePurchase"
                component={ProfilePurchase}
            />
        </stackProfilePurchase.Navigator>
    )
}

const donformstack = () => {
    return(
        <stackDonForm.Navigator
            initialRouteName="DonForm"
            screenOptions={{
                headerShown: false,
                headerHideShadow: true,
                headerStyle: {
                    backgroundColor: "#1ca7a0"
                }
            }}
        >
            <stackDonForm.Screen 
                name="Don"
                component={Don}
            />
        </stackDonForm.Navigator>
    )
}

const donliststack = () => {
    return(
        <stackDonList.Navigator
            initialRouteName="DonList"
            screenOptions={{
                headerShown: false,
                headerHideShadow: true,
                headerStyle: {
                    backgroundColor: "#1ca7a0"
                }
            }}
        >
            <stackDonList.Screen 
                name="DonList"
                component={DonList}
            />
        </stackDonList.Navigator>
    )
}

const basketprogressstack = () => {
    return(
        <stackBasketProgress.Navigator
            initialRouteName="BasketProgress"
            screenOptions={{
                headerShown: false,
                headerHideShadow: true,
                headerStyle: {
                    backgroundColor: "#1ca7a0"
                }
            }}
        >
            <stackBasketProgress.Screen 
                name="BasketProgress"
                component={BasketProgress}
            />
        </stackBasketProgress.Navigator>
    )
}

const aboutstack = () => {
    return(
        <stackAbout.Navigator
            initialRouteName="About"
            screenOptions={{
                headerShown: false,
                headerHideShadow: true,
                headerStyle: {
                    backgroundColor: "#1ca7a0"
                }
            }}
        >
            <stackAbout.Screen 
                name="About"
                component={About}
            />
        </stackAbout.Navigator>
    )
}

/* bottom tab-bar menu */
const BtmMenu = () => {
    return(
        <btmTab.Navigator
            initialRouteName="ShopScreen"
            tabBarOptions={{
                showLabel : false,
                showIcon : true,
                activeTintColor : '#ffffff',
                activeBackgroundColor : '#1ca7a0',
                inactiveTintColor : '#ffffff',
                inactiveBackgroundColor : '#066d68'
            }}
        >
            <btmTab.Screen 
                name="ShopScreen"
                component={Shop}
                options={{
                    tabBarIcon:({focused, color, size}) => {
                        return(
                            <Feathicon 
								name="shopping-bag" 
								size={25} 
								color={color}
							/>
                        )
                    }
                }}
            />
            <btmTab.Screen 
                name="Basket"
                component={basketprogressstack}
                options={{
                    tabBarIcon:({focused, color, size}) => {
                        return(
                            <Fonticon 
								name="shopping-cart" 
								size={25} 
								color={color}
							/>
                        )
                    }
                }}
            />
            <btmTab.Screen 
                name="Don"
                component={donSubMenu}
                options={{
                    tabBarIcon:({focused, color, size}) => {
                        return(
                            <Font5icon 
								name="donate" 
								size={25} 
								color={color}
							/>
                        )
                    }
                }}
            />
            <btmTab.Screen 
                name="Profile"
                component={profileSubMenu}
                options={{
                    tabBarIcon:({focused, color, size}) => {
                        return(
                            <Fonticon 
								name="user" 
								size={25} 
								color={color}
							/>
                        )
                    },
                    
                }}
            />
            <btmTab.Screen 
                name="AboutScreen"
                component={aboutstack}
                options={{
                    tabBarIcon:({focused, color, size}) => {
                        return(
                            <Font5icon 
								name="info-circle" 
								size={25} 
								color={color}
							/>
                        )
                    },
                    
                }}
            />
        </btmTab.Navigator>
    )
}

/* top sub-menu profile */
const profileSubMenu = () => {
    return(
        <topSubMenuProfile.Navigator
            initialRouteName="ProfileGeneralScreen"
            screenOptions={{
                activeBackgroundColor: 'red'
            }}
            tabBarOptions={{
                activeTintColor : '#ffffff',
                inactiveTintColor : '#000000',
                style: { backgroundColor: '#066d68' },
                indicatorStyle: {
                    height: 5,
                    backgroundColor: '#1ca7a0',
                    borderTopEndRadius: 2,
                    borderTopStartRadius: 2
                },
            }}
            lazy={true}
            removeClippedSubviews={true}
        >
            <topSubMenuProfile.Screen 
                name="ProfileGeneralScreen"
                component={generalprofilestack}
                options={{
                    title: ({color}) => {
                        return(
                            <Text style={{color: color}}>Général</Text>
                        )
                    },
                    tabBarAccessibilityLabel: "generalProfile",
                }}
            />
            <topSubMenuProfile.Screen 
                name="ProfileSecurityScreen"
                component={securityprofilestack}
                options={{
                    title: ({color}) => {
                        return(
                            <Text style={{color: color}}>Sécurité</Text>
                        )
                    },
                    tabBarAccessibilityLabel: "securityProfile"
                }}
            />
            <topSubMenuProfile.Screen 
                name="ProfilePurchaseScreen"
                component={purchaseprofilestack}
                options={{
                    title: ({color}) => {
                        return(
                            <Text style={{color: color}}>Achats</Text>
                        )
                    },
                    tabBarAccessibilityLabel: "purchaseProfile"
                }}
            />
        </topSubMenuProfile.Navigator>
    )
}

/* top submenu don */
const donSubMenu = () => {
    return(
        <topSubMenuDon.Navigator
            initialRouteName="DonFormScreen"
            tabBarOptions={{
                activeTintColor : '#ffffff',
                inactiveTintColor : '#000000',
                style: { backgroundColor: '#066d68' },
                indicatorStyle: {
                    height: 5,
                    backgroundColor: '#1ca7a0',
                    borderTopEndRadius: 2,
                    borderTopStartRadius: 2
                },
            }}
            lazy={true}
            removeClippedSubviews={true}
        >
            <topSubMenuDon.Screen 
                name="DonFormScreen"
                component={donformstack}
                options={{
                    title: ({color}) => {
                        return(
                            <Text style={{color: color}}>Don</Text>
                        )
                    },
                    tabBarAccessibilityLabel: "progressBasket",
                }}
            />
            <topSubMenuDon.Screen 
                name="DonListScreen"
                component={donliststack}
                options={{
                    title: ({color}) => {
                        return(
                            <Text style={{color: color}}>Liste des dons</Text>
                        )
                    },
                    tabBarAccessibilityLabel: "pendingBasket"
                }}
            />
        </topSubMenuDon.Navigator>
    )
}

/* navigator */
const StackNavigator = () => {
    return(
        <stackNative.Navigator
            initialRouteName="Login"
            screenOptions={{
                headerShown: true,
                headerHideShadow: true,
                headerStyle: {
                    backgroundColor: "#1ca7a0",
                },
                headerTintColor: "#ffffff",
                headerTitle: "Alt. Soap"
            }}
        >
            <stackNative.Screen 
                name="Login"
                component={Login}
                options={({navigation}) => ({
                    headerShown: false
                })}
            />
            <stackNative.Screen 
                name="Register"
                component={Register}
                options={({navigation}) => ({
                    headerShown: false
                })}
            />
            <stackNative.Screen 
                name="PurchaseDetail"
                component={PurchaseDetail}
            />
            <stackNative.Screen 
                name="ProductDetails"
                component={ProductDetails}
            />
            <stackNative.Screen 
                name="ForgotPassword"
                component={ForgotPassword}
            />
            <stackNative.Screen 
                name="PrincipalMenu"
                component={BtmMenu}
            />
            <stackNative.Screen 
                name="ConfirmCommand"
                component={ConfirmCommand}
            />
        </stackNative.Navigator>
    )
}

function LogoTitle(){
    return(
        <Image 
            style={{ width: 50, height: 50 }}
            source={require('../ressource/images/logo-altsoap.png')}
        />
    )
}

const styles = Styles

export default Navigation