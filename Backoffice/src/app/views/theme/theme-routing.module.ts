import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user/user.component';
import { UserAdminComponent } from './user/userAdmin.component';
import { UserClientComponent } from './user/userClient.component';
import { ProfilComponent } from './profil/profil.component';
import { AddUserComponent } from './user/addUser.component';
import { EditUserComponent } from './user/editUser.component';
import { ProductComponent } from './product/product.component';
import { AddProductComponent } from './product/addproduct.component';
import { EditProductComponent } from './product/editproduct.component';
import { OrderComponent } from './order/order.component';
import { OrderDetailComponent } from './order/orderdetail.component';
import { DonhuileComponent } from './donhuile.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Gestion'
    },
    children: [
      {
        path: '',
        redirectTo: 'allUsers'
      },
      {
        path: 'allUsers',
        component: UserComponent,
        data: {
          title: 'Utilisateurs'
        }
      },
      {
        path: 'user',
        component: ProfilComponent,
        data: {
          title: 'Profil'
        }
      },
      {
        path: 'useradmin',
        component: UserAdminComponent,
        data: {
          title: 'Administrateurs'
        }
      },
      {
        path: 'userclient',
        component: UserClientComponent,
        data: {
          title: 'Clients'
        }
      },
      {
        path: 'adduser',
        component: AddUserComponent,
        data: {
          title: 'Ajout utilisateur'
        }
      },
      {
        path: 'edituser',
        component: EditUserComponent,
        data: {
          title: 'Modifier utilisateur'
        }
      },
      {
        path: 'allorder',
        component: OrderComponent,
        data: {
          title: 'Commandes'
        }
      },
      {
        path: 'orderdetail',
        component: OrderDetailComponent,
        data: {
          title: 'Detail commande'
        }
      },
      {
        path: 'allproduct',
        component: ProductComponent,
        data: {
          title: 'Produits'
        }
      },
      {
        path: 'addproduct',
        component: AddProductComponent,
        data: {
          title: 'Ajouter produit'
        }
      },
      {
        path: 'editproduct',
        component: EditProductComponent,
        data: {
          title: 'Modifier produit'
        }
      },
      {
        path: 'donhuile',
        component: DonhuileComponent,
        data: {
          title: 'Don d\'huile'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThemeRoutingModule {}
