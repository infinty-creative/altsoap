// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { UserComponent } from './user/user.component';
import { UserAdminComponent } from './user/userAdmin.component';
import { UserClientComponent } from './user/userClient.component';
import { ProfilComponent } from './profil/profil.component';
import { AddUserComponent } from './user/addUser.component';
import { EditUserComponent } from './user/editUser.component';
import { OrderComponent } from './order/order.component';
import { OrderDetailComponent } from './order/orderdetail.component';
import { ProductComponent } from './product/product.component';
import { AddProductComponent } from './product/addproduct.component';
import { EditProductComponent } from './product/editproduct.component';
import { DonhuileComponent } from './donhuile.component';



// Theme Routing
import { ThemeRoutingModule } from './theme-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ThemeRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ProfilComponent,
    UserComponent,
    UserAdminComponent,
    UserClientComponent,
    EditUserComponent,
    AddUserComponent,
    OrderComponent,
    OrderDetailComponent,
    ProductComponent,
    AddProductComponent,
    EditProductComponent,
    DonhuileComponent
    ]
})
export class ThemeModule { }
