import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Home',
    url: '/admin/allorder',
    icon: 'fa fa-align-justify',
  },
  {
    title: true,
    name: 'Gestion'
  },
  {
    name: 'Utilisateurs',
    url: '/admin/alluser',
    icon: 'icon-user'
  },
  {
    name: 'Commandes',
    url: '/admin/allorder',
    icon: 'icon-basket'
  },
  {
    name: 'Produits',
    url: '/admin/allproduct',
    icon: 'icon-bag'
  },
  {
    name: 'Don d\'huile',
    url: '/admin/donhuile',
    icon: 'icon-drop'
  }
];
