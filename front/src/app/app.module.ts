import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog'

import {Routes, RouterModule} from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShoplistComponent } from './shoplist/shoplist.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FooterComponent } from './footer/footer.component';
import { ProductdetailComponent } from './productdetail/productdetail.component';
import { BackorderComponent } from './backorder/backorder.component';
import { PendingorderComponent } from './pendingorder/pendingorder.component';
import { PurchasemadeComponent } from './account/purchasemade/purchasemade.component';
import { AccountComponent } from './account/account.component';
import { SecurityComponent } from './account/security/security.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { MenuprofilComponent } from './account/menuprofil/menuprofil.component';
import { CommandeListComponent } from './pendingorder/commande-list/commande-list.component';
import { ListProductComponent } from './shoplist/list-product/list-product.component';
import { GeneralProfilComponent } from './account/general-profil/general-profil.component';
import { SecondFooterComponent } from './footer/second-footer/second-footer.component';

import { PayementComponent } from './pendingorder/payement/payement.component';
import { OrangemoneyComponent } from './pendingorder/payement/orangemoney/orangemoney.component';

import { AchatModalComponent } from './account/achat-modal/achat-modal.component';


const appRoutes: Routes = [
  {path: '', component: ShoplistComponent },
  {path: 'shoplist', component: ShoplistComponent },
  {path: 'productdetail', component: ProductdetailComponent },
  {path: 'backorder', component: BackorderComponent },
  {path: 'pendingorder', component: PendingorderComponent },
  {path: 'payement', component: PayementComponent },
  {path: 'orangemoney', component: OrangemoneyComponent },
  {path: 'purchasemade', component: PurchasemadeComponent },
  {path: 'account', component: AccountComponent },
  {path: 'security', component: SecurityComponent },
  {path: 'login', component: LoginComponent },
  {path: 'register', component: RegisterComponent },
  {path: '**', redirectTo: '/notFound', pathMatch: 'full'}

];

@NgModule({
  declarations: [
    AppComponent,
    ShoplistComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    FooterComponent,
    ProductdetailComponent,
    BackorderComponent,
    PendingorderComponent,
    PurchasemadeComponent,
    AccountComponent,
    SecurityComponent,
    NotfoundComponent,
    MenuprofilComponent,
    CommandeListComponent,
    ListProductComponent,
    GeneralProfilComponent,
    SecondFooterComponent,
    PayementComponent,
    OrangemoneyComponent,
    AchatModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[AchatModalComponent]
})
export class AppModule { }
