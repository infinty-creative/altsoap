import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent  {
  showSideMenu = true;

  /* Show sub menu (si responsive)*/
  ShowSideMenuAction(){
    this.showSideMenu = !this.showSideMenu;
  }
  hideSideMenu(){
    this.showSideMenu = !this.showSideMenu;
  }


  logout(){

  }


}
