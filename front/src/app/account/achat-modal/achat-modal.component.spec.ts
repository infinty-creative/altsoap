import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AchatModalComponent } from './achat-modal.component';

describe('AchatModalComponent', () => {
  let component: AchatModalComponent;
  let fixture: ComponentFixture<AchatModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchatModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AchatModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
