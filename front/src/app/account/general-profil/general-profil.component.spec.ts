import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralProfilComponent } from './general-profil.component';

describe('GeneralProfilComponent', () => {
  let component: GeneralProfilComponent;
  let fixture: ComponentFixture<GeneralProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
