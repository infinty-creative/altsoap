<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('login','API\UserController@login');
Route::post('register','API\UserController@register');

Route::group(['middleware'=>'auth:api'], function(){
    Route::post('detail','API\UserController@detail');
    Route::post('admin','API\UserController@addadmin');
    Route::apiResource('/user','ProfilController');
    Route::apiResource('/packs','PackController');
    

});
Route::apiResource('/user','ProfilController');
Route::apiResource('/categories','CategoryController');

Route::group(['prefix'=>'categories'],function(){
    Route::apiResource('/{category}/products','ProductController');
    //Route::apiResource('/{category}/products/{product}/packs','PackController');
});




