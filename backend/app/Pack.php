<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    protected $fillable = [
        'statuts',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    } 
}
