<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Code_Categorie' =>$this->codeCategory,
            'Nom_Categorie' =>$this->nameCategory,
            'href'=>[
                'products'=>route('products.index',$this->id)
            ] 
        ];
    }
}
