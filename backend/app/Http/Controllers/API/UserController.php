<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Profil;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public $successStatus = 200;
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */
    
     public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['activated'] = $user->activated;
            $success['userId'] = $user->id;
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
    /**
    * Register api 
    * 
    * @return \Illuminate\Http\Response 
    */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 'string', 'max:255',
            'lastname' => 'required', 'string', 'max:255',
            'country' => 'required', 'string', 'max:255',
            'address' => 'required', 'string', 'max:255',
            'phone' => 'required', 'string', 'max:255',
            'email' => 'required', 'string', 'email', 'unique:users',
            'password' => 'required', 'string', 'min:8', 'confirmed',
            'c_password' => 'required|same:password',
            'type' => 'required', 'boolean',
            
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $checkmail = User::where('email', $request['email'])->first();
        if($checkmail){
            $user=User::create([
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'type' => $request['type'],
                'activated'=>1,
            ]); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['userId'] =  $user->id;
            //$success['activated'] =  $user->activated;
            $user->profil()->create($input);
            return response()->json(['success'=>$success], $this-> successStatus);
        }else{
            return response()->json(['error'=>'Email already exist'], 401); 
        }
        
        /*DB::table('users')
    			->join('profils','profils.user_id','users.id')
                ->selectRaw('users.*,lastname,country,address,phone,user_id')
    			->where('users.id',$user->id)
    			->get();*/
    }

    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function detail() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 

    public function addadmin() 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 'string', 'max:255',
            'lastname' => 'required', 'string', 'max:255',
            'country' => 'required', 'string', 'max:255',
            'address' => 'required', 'string', 'max:255',
            'phone' => 'required', 'string', 'max:255',
            'email' => 'required', 'string', 'email', 'unique:users',
            'password' => 'required', 'string', 'min:8', 'confirmed',
            'c_password' => 'required|same:password',
            'type' => 'required', 'boolean',
            
        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $user=User::create([
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'type' => $request['type'],
            'activated'=>1,
        ]); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['userId'] =  $user->id;
        //$success['activated'] =  $user->activated;
        $user->profil()->create($input);
        return response()->json(['success'=>$success], $this-> successStatus);
    } 
}
