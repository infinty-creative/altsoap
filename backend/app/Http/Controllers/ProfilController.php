<?php

namespace App\Http\Controllers;

use App\Http\Resources\Profil\ProfilResource;
use App\Profil;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        
    }
    
     public function index()
    {
        $users=DB::table('users')
    	->join('profils','profils.user_id','users.id')
        ->selectRaw('users.id,name,lastname,country,address,users.email,phone')
    	->paginate(20);
        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users=DB::table('users')
    	->join('profils','profils.user_id','users.id')
        ->selectRaw('users.id,name,lastname,country,address,users.email,phone')
        ->where('users.id',$id)
    	->get();
        return new ProfilResource($users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function edit(Profil $profil)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req,$id)
    {
        $user=User::find($id);
        $profil=Profil::where('user_id',$user->id)->first();
        if($req->email){
            $user->email=$req->email;
        }
        if($req->type){
            $user->type=$req->type;
        }
        if($req->activated){
            $user->activated=1;
        }
        if($req->name){
            $profil->name=$req->name;
        }
        if($req->lastname){
            $profil->lastname=$req->lastname;
        }
        if($req->country){
            $profil->country=$req->country;
        }
        if($req->address){
            $profil->address=$req->address;
        }
        if($req->phone){
            $profil->phone=$req->phone;
        }
        if($req->oldpassword && $req->newpassword){
            $checkpassword = Hash::check($req->oldpassword, $user->password);
            if($checkpassword){
                $user->password=Hash::make($req->newpassword);
            }else{
                return response()->json(['error'=>'Wrong Password'], 401);           
            }
        }
    	$user->save();
    	// $profil->user_id=$user->id;
    	$profil->save();
    	$userUpdated=DB::table('users')
    			->join('profils','profils.user_id','users.id')
                ->selectRaw('users.id,name,lastname,country,address,users.email,phone')
    			->where('users.id',$user->id)
    			->get();
    	return new ProfilResource($userUpdated);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userDeleted=DB::table('users')
    	->join('profils','profils.user_id','users.id')
        ->selectRaw('users.id,name,lastname,country,address,users.email,phone')
    	->where('users.id',$id)
    	->get();
    	$profile=Profil::where('user_id',$id)->first();
    	$profile->delete();
    	$user=User::find($id);
    	$user->delete();
    	return new ProfilResource($userDeleted);
    }
}
