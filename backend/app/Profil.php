<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $fillable = [
        'name', 'lastname', 'country','address','phone',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    } 
}
