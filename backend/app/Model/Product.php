<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'nameProduct', 'price', 'description','image',
    ];
    
    public function category()
    {
        return $this->belongsTo(Category::class);
    } 
}
