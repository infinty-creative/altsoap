<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        factory(App\User::class,3)->create();
        factory(App\Profil::class,3)->create();
        factory(App\Model\Category::class,3)->create();
        factory(App\Model\Product::class,10)->create();
    }
}
