<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Product;
use App\Model\Category;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'category_id' => function(){
            return Category::all()->random();
        },
        'nameProduct' =>$faker->word,
        'price' => $faker->numberBetween(1000,5000),
        'description' =>$faker->paragraph,
        'image' =>$faker->paragraph,
    ];
});
