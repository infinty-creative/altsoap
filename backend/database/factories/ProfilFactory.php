<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Profil;
use App\User;
use Faker\Generator as Faker;

$factory->define(Profil::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return User::all()->random();
        },
        'name' =>$faker->name,
        'lastname' => $faker->word,
        'country' =>$faker->word,
        'address' =>$faker->address,
        'phone' =>$faker->randomNumber,
    ];
});
